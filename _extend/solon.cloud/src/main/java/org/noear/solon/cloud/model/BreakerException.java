package org.noear.solon.cloud.model;

/**
 * @author noear
 * @since 1.3
 */
public class BreakerException extends Exception {
    public BreakerException() {
        super();
    }

    public BreakerException(Throwable cause) {
        super(cause);
    }
}
